import React from "react";
import { useTranslation } from "react-i18next";
import { useRecoilValue } from "recoil";

import { GridList } from "components/ui-own";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "components/ui-libraries";
import { dashboardState } from "../../../atom/data-dashboard.atom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      width: 300,
    },
    body: {
      width: "100%",
      height: "90vh",
    },
  })
);

const CategoryDialog = React.memo(({ open, handleClose, onItemClick }: any) => {
  const classes = useStyles();
  const { t } = useTranslation(["common", "project"]);
  const { firstLoad, categories } = useRecoilValue(dashboardState);

  return (
    <Dialog maxWidth={"lg"} fullWidth onClose={handleClose} open={open}>
      <DialogTitle>{t("categories")}</DialogTitle>
      <DialogContent classes={{ root: classes.body }}>
        <GridList
          loading={firstLoad}
          items={categories}
          onItemClick={onItemClick}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          {t("close")}
        </Button>
      </DialogActions>
    </Dialog>
  );
});

export default CategoryDialog;
