import React, {useCallback, useState} from "react";
import {useTranslation} from "react-i18next";

import {Button} from "components/ui-libraries";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

import CategoryDialog from "./category-dialog.component";
import {useProjectItemContext} from "../../../project-item.context";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      width: "100%",
      height: 150,
      borderStyle: "dashed",
    },
    container: {
      padding: theme.spacing(3),
    },
  })
);

const Category = () => {
  const { t } = useTranslation("project");
  const classes = useStyles();
  const {
    setState: setStateGlobal,
  } = useProjectItemContext();
  const [state, setState] = useState({
    open: false,
  });

  const handleDialogOpen = useCallback(() => {
    setState((prevState) => ({ ...prevState, open: true }));
  }, []);

  const handleDialogClose = useCallback(() => {
    setState((prevState) => ({ ...prevState, open: false }));
  }, []);

  const onItemClick = useCallback(
    ({ item }) => {
      setStateGlobal((draft: any) => {
        draft.data.config.layout[draft.history.layout.name].components[
          draft.history.component
        ] = [item.id];
      });

      handleDialogClose();
    },
    [setStateGlobal, handleDialogClose]
  );

  return (
    <div className={classes.container}>
      <Button
        className={classes.button}
        variant="outlined"
        color="primary"
        onClick={handleDialogOpen}
      >
        {t("add_category")}
      </Button>
      <CategoryDialog
        handleClose={handleDialogClose}
        open={state.open}
        onItemClick={onItemClick}
      />
    </div>
  );
};

export default Category;
